import { Request, Response } from 'express';

const lastResortErrorHandler = (err: Error, req: Request, res: Response): void => {
  const traceId = req.context ? req.context.traceId : 'none';
  console.error(traceId, err);
  res.status(500).send('error');
};

export default lastResortErrorHandler;
