import { Express } from 'express';
import { AppConfig } from '../config';

const mountRoutes = (app: Express, config: AppConfig): void => {
  app.use('/', config.homeApp.router);
  app.use('/record-viewing', config.recordViewingsApp.router);
};

export default mountRoutes;
