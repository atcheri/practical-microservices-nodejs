// import camelCase from "camelcase-keys"
import express, { NextFunction, Request, Response } from 'express';

type RecordViewingActions = {
  recordViewing: (traceId: string, videoId: string) => Promise<void>;
};

const createActions = ({ db }: { db: any }): RecordViewingActions => {
  const recordViewing = async (traceId: string, videoId: string) => {
    return;
  };
  return {
    recordViewing
  };
};

const createHandlers = ({ actions }: { actions: RecordViewingActions }) => {
  const handleRecordViewing = ({ context: { traceId }, params }: Request, res: Response, next: NextFunction) => {
    return actions
      .recordViewing(traceId, params.videoId)
      .then(() => res.redirect('/'))
      .catch(next);
  };
  return {
    handleRecordViewing
  };
};

const createRecordViewings = ({ db }: { db: any }) => {
  const actions = createActions({ db });
  const handlers = createHandlers({ actions });
  const router = express.Router();
  router.route('/:videoId').post(handlers.handleRecordViewing);

  return {
    actions,
    handlers,
    router
  };
};

export default createRecordViewings;
