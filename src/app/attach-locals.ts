import { NextFunction, Request, Response } from 'express';

const attachLocals = (req: Request, res: Response, next: NextFunction): void => {
  res.locals.context = req.context;
  next();
};

export default attachLocals;
