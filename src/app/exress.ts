import express, { Express } from 'express';
import { join } from 'path';
import { AppEnvs } from '../app/env';
import mountRoutes from './mount-routes';

// eslint-disable-next-line
const createExpressApp = ({ config, env }: { config: any; env: AppEnvs }): Express => {
  const app = express();

  mountRoutes(app, config);

  // Configure PUG
  app.set('views', join(__dirname, '..'));
  app.set('view engine', 'pug');

  return app;
};

export default createExpressApp;
