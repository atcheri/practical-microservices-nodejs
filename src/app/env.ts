const requireFromEnv = (field: string): string => process.env[field] || field;

import packageJson from '../../package.json';

export type AppEnvs = {
  appName: string;
  databaseUrl: string;
  mode: string;
  port: number | string;
  version: number | string;
};

export const appName = requireFromEnv('APP_NAME');
export const databaseUrl = requireFromEnv('DATABASE_URL');
export const mode = requireFromEnv('NODE_ENV');
export const port = parseInt(requireFromEnv('PORT'), 10) || 3000;
export const version = packageJson.version;

const appEnvs: AppEnvs = {
  databaseUrl,
  appName,
  mode,
  port,
  version
};

export default appEnvs;
