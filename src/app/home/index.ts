// import camelCase from "camelcase-keys"
import express, { NextFunction, Request, Response } from 'express';

type CreateQueriesType = {
  loadHomePage: () => Promise<number>;
};

const createHandlers = ({ queries }: { queries: CreateQueriesType }) => {
  const home = (req: Request, res: Response, next: NextFunction) => {
    return queries
      .loadHomePage()
      .then((viewData) => res.render('home/templates/home' /*viewData*/))
      .catch(next);
  };

  return {
    home
  };
};

const createQueries = ({ db }: { db: any }) => {
  const loadHomePage = (): Promise<number> => {
    return db.then((client: any) =>
      client('videos')
        .sum('view_count as videosWatched')
        .then((rows: number[]) => rows[0])
    );
  };
  return {
    loadHomePage
  };
};

const createHome = ({ db }: { db: any }) => {
  const queries = createQueries({ db });
  const handlers = createHandlers({ queries });
  const router = express.Router();
  router.route('/').get(handlers.home);

  return {
    handlers,
    queries,
    router
  };
};

export default createHome;
