import { NextFunction, Request, Response } from 'express';
import { v4 } from 'uuid';

const primeRequestContext = (req: Request, res: Response, next: NextFunction): void => {
  req.context = {
    traceId: v4()
  };
  next();
};

export default primeRequestContext;
