import env from './app/env';
import createExpressApp from './app/exress';
import createConfig from './config';

export const config = createConfig(env);
export const app = createExpressApp({ config, env });

export const startApp = (): void => {
  app.listen(env.port, signalAppStart);
};

const signalAppStart = () => {
  console.log(`${env.appName} started`);
  console.table([
    ['Port', env.port],
    ['Environment', env.mode]
  ]);
};
