import knex, { Knex, KnexConfig } from 'knex';

const createKnexClient = async ({
  connectionConfig,
  migrationTableName
}: {
  // connectionString: string;
  connectionConfig: KnexConfig;
  migrationTableName?: string;
}): Promise<Knex> => {
  const client = knex(connectionConfig);

  await client.migrate.latest({
    tableName: migrationTableName || 'migrations'
  });

  return client;
};

export default createKnexClient;
