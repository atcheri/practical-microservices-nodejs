declare namespace Express {
  export interface Request {
    context: {
      traceId: string;
    };
  }
}
