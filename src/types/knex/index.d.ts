import 'knex';

declare module 'knex' {
  export type KnexConfig = Knex.Config;
}
