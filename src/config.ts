import knexConfigs from '../knexfile';
import { AppEnvs } from './app/env';
import createHome from './app/home';
import createRecordViewings from './app/record-view';
import createKnexClient from './knex-client';

export type AppConfig = {
  env: AppEnvs;
  db: any;
  homeApp: any;
  recordViewingsApp: any;
};

const createConfig = (env: AppEnvs): AppConfig => {
  const db = createKnexClient({
    // connectionString: env.databaseUrl
    connectionConfig: knexConfigs.development
  });
  const homeApp = createHome({ db });
  const recordViewingsApp = createRecordViewings({ db });
  return {
    env,
    db,
    homeApp,
    recordViewingsApp
  };
};

export default createConfig;
